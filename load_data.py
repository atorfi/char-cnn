import json

data_path = 'data/arabic_news/autogsr_arabic_data_feb16_jul16.json'
alphabet_path = 'alphabet_arabic.json'

with open(alphabet_path) as alphabet_file:
    alphabet = json.load(alphabet_file)
        
# open json file
json_data=open(data_path)

# read one line
msg = json.loads(json_data.readline())

# pring the keys of the dictionary
print(msg.keys())

# print the title
print(msg['title'])
#
#chars_uni = []
#for char in msg['text']:
#    chars_uni.append(char)
#
#unique = list(set(chars_uni))
#print(len(unique))

## Uncomment if further reading all articles is required becasue it's memory consuming.

## Open json file
#json_data=open(data_path)
#
## Load all articles and form a list
#articles = [json.loads(ln) for ln in json_data]
#
## Print a random article title in unicode
#article = articles[random.randrange(len(articles))]
#print("First article title:", article['title'])
#
## Print first article title plain and without unicode
#print(article['title'])
#
## Get the label by extracting the events
#print(article['finalEvents'][0]['eType'])
#
## Gather the protest articles
#protest_articles = [article for article in articles if article['finalEvents'][0]['eType']!='Non-Protest Article']
#print('Number of protest articles={} total={} '.format(len(protest_articles),len(articles)))