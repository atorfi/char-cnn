import csv
import torch
import json
import sys
from IPython.core.debugger import set_trace
from torch.utils.data import DataLoader, Dataset
import random

class AGNEWs(Dataset):
    def __init__(self, samples, alphabet_path, phase, balanced_classes, l0=1014):
        """Create AG's News dataset object.

        Arguments:
            label_data_path: The path of label and data file in csv.
            l0: max length of a sample.
            alphabet_path: The path of alphabet json file.
        """
#         self.label_data_path = label_data_path
#         # read alphabet
        with open(alphabet_path) as alphabet_file:
            alphabet = json.load(alphabet_file)
        self.alphabet = alphabet
        self.phase = phase
        self.balanced_classes = balanced_classes
        self.samples = samples
        self.l0 = l0
        self.load()
        self.y = torch.LongTensor(self.label)
            
    def __len__(self):
        return len(self.label)

    def __getitem__(self, idx):
        X = self.oneHotEncode(idx)
        y = self.y[idx]
        return X, y


    def load(self, lowercase=True):
        self.label = []
        self.data = []
        
        articles  = self.samples
        number_of_samples = len(articles)

        for idx in range(number_of_samples):

            ####################
            ### Feeding data ###
            ####################
            article = articles[idx]

            # Extract the context
            context = article['text']
            space =[u' ']
            # Eliminate space
            context = [x for x in context if x != space[0]]
            self.data.append(context)

            #####################
            ### Feeding label ###
            #####################

            label_str = article['finalEvents'][0]['eType']
            if label_str == 'Non-Protest Article':
              self.label.append(int(0))
            elif label_str == 'Out-Country Protest Article' or label_str == 'In-Country Protest Article':
              self.label.append(int(1))
            else:
              sys.exit('Non-defined label')

 

    def oneHotEncode(self, idx):
        # X = (batch, 70, sequence_length)
        X = torch.zeros(len(self.alphabet), self.l0)
        sequence = self.data[idx]
        for index_char, char in enumerate(sequence[:self.l0]):
            if self.char2Index(char)!=-1:
                X[self.char2Index(char)][index_char] = 1.0
        return X

    def char2Index(self, character):
        
        try:
            return self.alphabet.index(character)
        except:
            return -1

    def get_class_weight(self):
        num_samples = self.__len__()
        label_set = set(self.label)
        num_class = [self.label.count(c) for c in label_set]
        class_weight = [num_samples/float(self.label.count(c)) for c in label_set]    
        return class_weight, num_class
