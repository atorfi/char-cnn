import json

data_path = 'data/arabic_news/autogsr_arabic_data_feb16_jul16.json'
alphabet_path = 'alphabet_arabic.json'

alphabet = []
with open(alphabet_path) as alphabet_file:
    for char in json.load(alphabet_file):
        alphabet.append(char)

print(alphabet)

# Open json file
json_data=open(data_path)

# Load all articles and form a list
articles = [json.loads(ln) for ln in json_data]

# Extract the unique characters in all articles
chars_unique = []
for article in articles:
  context = article['text']
  space =[u' ']
  # Eliminate space
  context = [x for x in context if x != space[0]]
  for char in context:
    chars_unique.append(char)
  chars_unique = list(set(chars_unique))

# Create a dictionary with characters
#     Keys: Characters 
#     Valuse: The number of occurance of the characters
dictionary = dict()
for article in articles:
  context = article['text']
  space =[u' ']
  context = [x for x in context if x != space[0]]
  for char in chars_unique:
    if char in dictionary:
       dictionary[char] += context.count(char)
    else:
       dictionary[char] = context.count(char)

# Sort the dictionry based on the values (ascending order).
sorted_dictionary = sorted(dictionary.items(), key=lambda t: t[1])
chosen_chars = sorted_dictionary[-70:]

char_list = []
with open('data.json', 'w') as f:
  # print(sorted_dictionary[-1])
  for idx in range(len(chosen_chars)):
    print(chosen_chars[idx][0])
    char_list.append(chosen_chars[idx][0])
  json.dump(char_list, f)